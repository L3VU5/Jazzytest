app.factory("gnomes", ["$http", function($http){
	return $http.get("http://master.datasource.jazzy-hr.jzapp.io/api/v1/gnomes?_format=json&limit=20&offset=1")
	.success(function(data){
		return data;
	})
	.error (function(data){
		return data;
	});
}]);