
<!DOCTYPE html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<link href="style.css" type="text/css" rel="stylesheet"/>
<title>jazzyapp</title>
</head>
<body ng-app="myApp">
	<div class="container main">
		<div class="row topline">
			<div class="col-lg-2">
				<a href="/"><img src="assets/logo.png"/></a>
			</div>
			<div class="col-lg-1"></div>
			<div class="col-lg-1 links"><b>Gnoms</b></div>
			<div class="col-lg-1 links"><b>Trolls</b></div>
			<div class="col-lg-3"></div>
			<div class="col-lg-2">
				<button type="button" data-toggle="modal" data-target="#myModal2" class="btn btn-primary btn-md">Create monster</button>
			</div>
			<div class="col-lg-2 row">
				<div class="col-lg-4"><img src="assets/avatar.png"/></div>
				<div class="col-lg-8">
					<div class="user">
						<p style="font-size:11px;"><b>User Name</b></p>
						<p style="font-size: 10px;">Game Master</p>
					</div>
				</div>
			</div>
		</div>
		<h3>Gnomes</h3>
		<div class="mid" ng-controller="MainController">
			<div class="gnomes" ng-repeat="gnome in gnomes">
				<div type="button" class=" gnome row" data-toggle="modal" data-target="#myModal">
					<div class="col-lg-1"><img src="assets/avatar.png" style="float:right;" /></div>
					<div class="col-lg-7 nazwa">
						<div><b>{{gnome.name}}</b></div>
						<div><small>Age: {{gnome.age}}</small></div>
					</div>
					<div class="col-lg-2 pasek"><div class="stan" style="width:{{gnome.strenght}}%;"></div></div>
					<div class="col-lg-1">{{gnome.strenght}}/100</div>
					<div class="col-lg-1"><b>Strenght</b></div>
				</div>
			</div>
			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Change gnome name...</h4>
						</div>
						<div class="modal-body input">
							<input type="text" name="name" placeholder="New name"></input>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="myModal2" role="dialog" ng-controller="MainController">
				<div class="modal-dialog modal-md">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Create new gnome</h4>
						</div>
						<div class="modal-body row">
						<div class="col-md-4">
							<span>Enter name: </span>
						</div>
						<div class="col-md-8">
							<input class="modalinput" type="text" name="name" placeholder="Name"></input>
						</div>
						<div class="col-md-4">
							<span>Enter age: </span>
						</div>
						<div class="col-md-8">
							<input class="modalinput" type="text" name="age" placeholder="Age"></input>
						</div>
						<div class="col-md-4">
							<span>Enter strenght" </span>
						</div>
						<div class="col-md-8">
							<input class="modalinput" type="text" name="strenght" placeholder="Strenght"></input>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" onclick="addGnome()" class="btn btn-default" data-dismiss="modal">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
		
	<script src="lib/angular.js"></script>
	<!-- Modules -->
    <script src="js/myApp.js"></script>

    <!-- Controllers -->
    <script src="js/controllers/MainController.js"></script>
	
    <!-- Services -->
    <script src="js/services/gnomes.js"></script>
</body>
</html>